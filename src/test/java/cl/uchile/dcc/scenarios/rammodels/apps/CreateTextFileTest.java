/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.scenarios.rammodels.apps;

import java.io.File;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class CreateTextFileTest {
    
    /**
     *
     */
    public CreateTextFileTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getFileName method, of class CreateTextFile.
     */
    @Test
    public void testGetFileName() {
        out.println("getFileName");
        CreateTextFile instance = new CreateTextFile("temp/hola.txt", "Hello World");
        String expResult = "temp/hola.txt";
        String result = instance.getFileName();
        assertEquals(expResult, result);
    }

    /**
     * Test of setFileName method, of class CreateTextFile.
     */
    @Test
    public void testSetFileName() {
        out.println("setFileName");
        String fileName = "temp/hola2.txt";
        CreateTextFile instance = new CreateTextFile("temp/hola.txt", "Hello World");
        instance.setFileName(fileName);
        assertTrue(fileName.equals(instance.getFileName()));
    }

    /**
     * Test of getTexto method, of class CreateTextFile.
     */
    @Test
    public void testGetTexto() {
        out.println("getTexto");
        CreateTextFile instance = new CreateTextFile("temp/hola.txt", "Hello World");
        String expResult = "Hello World";
        String result = instance.getTexto();
        assertEquals(expResult, result);
    }

    /**
     * Test of setTexto method, of class CreateTextFile.
     */
    @Test
    public void testSetTexto() {
        out.println("setTexto");
        String texto = "Hola Mundo";
        CreateTextFile instance = new CreateTextFile("temp/hola.txt", "Hello World");
        instance.setTexto(texto);
        assertTrue(texto.equals(instance.getTexto()));
    }

    /**
     * Test of create method, of class CreateTextFile.
     */
    @Test
    public void testCreate() {
        out.println("create");
        CreateTextFile instance = new CreateTextFile("c:/temp/hola.txt", "Hello World");
        instance.create();
        File f = new File("c:/temp/hola.txt");
        assertTrue(f.exists() && !f.isDirectory());
    }    
}
