/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.scenarios.rammodels.apps;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.File;
import java.io.IOException;
import static java.lang.System.out;
import static java.nio.file.Files.write;
import static java.nio.file.StandardOpenOption.CREATE;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class ModelFilesReaderTest {
    private List<Pair<Double>> marco = new ArrayList<>();
    private List<Pair<Double>> listaVelocidades = new ArrayList<>();
    private List<List<Point2D>> listaPuntosObjetos = new ArrayList<>();
    private Pair<Double> intervaloTiempo;
    private int fps;
    private File archivo = null;

    /**
     *
     */
    @Rule
    public TemporaryFolder testFolder = new TemporaryFolder();
    
    /**
     *
     */
    public ModelFilesReaderTest() {
    }
    
    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    
    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        List<Point2D> listaPuntosTemp;
        this.testFolder.create();
        
        this.archivo = this.testFolder.newFile("prueba.txt");
        this.archivo.setWritable(true);
        List<String> listaTexto = new ArrayList<>();
        
        listaTexto.add("marco:(-10.0,10.0),(-10.0,10.0)");
        this.marco.add(new PairDouble(-10.0, 10.0));
        this.marco.add(new PairDouble(-10.0, 10.0));
        
        listaTexto.add("timeInterval:0.0-10.0");
        this.intervaloTiempo = new PairDouble(0,10);
        
        listaTexto.add("framesPerSecond:10");
        this.fps = 10;
        
        listaTexto.add("1.0 -1.0");
        listaTexto.add("4 -3.0 1.0 -1.0 1.5 -5.0 2.0 -5.0 1.5");
        this.listaVelocidades.add(new PairDouble(1.0, -1.0));
        listaPuntosTemp = new ArrayList<>(4);
        listaPuntosTemp.add(new Point2D(-3.0, 1.0));
        listaPuntosTemp.add(new Point2D(-1.0, 1.5));
        listaPuntosTemp.add(new Point2D(-5.0, 2.0));
        listaPuntosTemp.add(new Point2D(-5.0, 1.5));
        this.listaPuntosObjetos.add(listaPuntosTemp);
        
        listaTexto.add("-2.0 0.0");
        listaTexto.add("3 3.0 -2.0 2.0 2.0 1.0 -2.0");
        this.listaVelocidades.add(new PairDouble(-2.0, 0.0));
        listaPuntosTemp = new ArrayList<>(3);
        listaPuntosTemp.add(new Point2D(3.0, -2.0));
        listaPuntosTemp.add(new Point2D(2.0, 2.0));
        listaPuntosTemp.add(new Point2D(1.0, -2.0));
        this.listaPuntosObjetos.add(listaPuntosTemp);
        
        listaTexto.add("-1.0 2.0");
        listaTexto.add("4 -3.0 -4.0 -1.0 -3.0 -2.0 -2.0 -3.0 -2.0");
        this.listaVelocidades.add(new PairDouble(-1.0, 2.0));
        listaPuntosTemp = new ArrayList<>(4);
        listaPuntosTemp.add(new Point2D(-3.0, -4.0));
        listaPuntosTemp.add(new Point2D(-1.0, -3.0));
        listaPuntosTemp.add(new Point2D(-2.0, -2.0));
        listaPuntosTemp.add(new Point2D(-3.0, -2.0));
        this.listaPuntosObjetos.add(listaPuntosTemp);
        
        write(this.archivo.toPath(), listaTexto, CREATE);
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
        this.archivo.delete();
        this.testFolder.delete();
    }

    /**
     * Test of readFile method, of class ModelFilesReader.
     * @throws java.lang.Exception
     */
    @Test
    public void testReadFile_direct() throws Exception {
        out.println("readFile: direct");
        String fileName = this.archivo.getPath();
        ModelFilesReader instance = new ModelFilesReader(fileName, "direct", true);
        SimulationStaticDirectSweepAndPrune expResult = new SimulationStaticDirectSweepAndPrune(this.listaPuntosObjetos, this.marco, this.listaVelocidades, this.fps, this.intervaloTiempo.getFirst(), this.intervaloTiempo.getSecond(), true);
        SimulationStaticDirectSweepAndPrune result = (SimulationStaticDirectSweepAndPrune)instance.readFile(fileName);
        assertEquals(expResult.getDimensiones(), result.getDimensiones());
        assertEquals(expResult.getInitialPointsListOfObjects(), result.getInitialPointsListOfObjects());
        assertEquals(expResult.getSpeedList(), result.getSpeedList());
    }

    /**
     * Test of readFile method, of class ModelFilesReader.
     * @throws java.lang.Exception
     */
    @Test
    public void testReadFile_incremental() throws Exception {
        out.println("readFile: incremental");
        String fileName = this.archivo.getPath();
        ModelFilesReader instance = new ModelFilesReader(fileName, "incremental", true);
        SimulationStaticIncrementalSweepAndPrune expResult = new SimulationStaticIncrementalSweepAndPrune(this.listaPuntosObjetos, this.listaVelocidades, this.fps, this.intervaloTiempo.getFirst(), this.intervaloTiempo.getSecond(), true);
        SimulationStaticIncrementalSweepAndPrune result = (SimulationStaticIncrementalSweepAndPrune)instance.readFile(fileName);
        assertEquals(expResult.getObjectsList(), result.getObjectsList());
        assertEquals(expResult.getSpeedList(), result.getSpeedList());
        // probar mas detallado el rsp de c/u
        //assertEquals(expResult.getRsp(), result.getRsp());
        assertEquals(expResult.getRsp().getCollisionsList(), result.getRsp().getCollisionsList());
        assertEquals(expResult.getRsp().getEndPointsX(), result.getRsp().getEndPointsX());
        assertEquals(expResult.getRsp().getEndPointsY(), result.getRsp().getEndPointsY());
        assertEquals(expResult.getRsp().getListaCajas(), result.getRsp().getListaCajas());
        assertEquals(expResult.getRsp().getListaColisionesX(), result.getRsp().getListaColisionesX());
        assertEquals(expResult.getRsp().getListaColisionesY(), result.getRsp().getListaColisionesY());
        assertEquals(expResult.getRsp().getListaVelocidades(), result.getRsp().getListaVelocidades());
        assertEquals(expResult.getRsp().getPosicionesActualesX(), result.getRsp().getPosicionesActualesX());
        assertEquals(expResult.getRsp().getPosicionesActualesY(), result.getRsp().getPosicionesActualesY());
        assertEquals(expResult.getRsp().getSpeedList(), result.getRsp().getSpeedList());
        assertEquals(expResult.getTimeStep(), result.getTimeStep(), 0.0001);
    }

    /**
     * Test of readFile method, of class ModelFilesReader.
     * @throws java.lang.Exception
     */
    @Test
    public void testReadFile_kinetic() throws Exception {
        out.println("readFile: kinetic");
        String fileName = this.archivo.getPath();
        ModelFilesReader instance = new ModelFilesReader(fileName, "kinetic", true);
        
        SimulationKinetic expResult = new SimulationKinetic(this.listaPuntosObjetos, this.listaVelocidades, this.intervaloTiempo.getFirst(), this.intervaloTiempo.getSecond(), this.marco, true);
        SimulationKinetic result = (SimulationKinetic)instance.readFile(fileName);
        assertEquals(expResult.getTinicial(), result.getTinicial(), 0.0001);
        assertEquals(expResult.getTfinal(), result.getTfinal(), 0.0001);
        //assertArrayEquals(expResult.getListaObjetos().toArray(), result.getListaObjetos().toArray());
    }
    
}
