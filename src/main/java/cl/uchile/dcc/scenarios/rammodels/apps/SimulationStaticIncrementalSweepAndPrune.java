/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.scenarios.rammodels.apps;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Double.doubleToLongBits;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import cl.uchile.dcc.staticlibrary.sap.IncrementalSweepAndPrune;
import cl.uchile.dcc.staticlibrary.sap.Statistics;
import cl.uchile.dcc.staticlibrary.sap.StatisticsIncrementalSP;

/**
 *
 * @author hmoraga
 */
public class SimulationStaticIncrementalSweepAndPrune implements Simulation {

    private final boolean verbose;
    private IncrementalSweepAndPrune rsp;
    private final int timeStep;
    private List<List<Point2D>> listaObjetos;
    private final List<Pair<Double>> listaVelocidades;
    private final double tinicial, tfinal;
    private StatisticsIncrementalSP rspStats;
    private List<Pair<Integer>> listaColisionesActual, listaColisionesAnterior;

    /**
     *
     * @param listaPuntosObjetos
     * @param listaVelocidades
     * @param timeStep
     * @param tinicial
     * @param tfinal
     * @param verbose
     */
    public SimulationStaticIncrementalSweepAndPrune(List<List<Point2D>> listaPuntosObjetos, List<Pair<Double>> listaVelocidades, int timeStep, double tinicial, double tfinal, boolean verbose) {
        this.listaObjetos = listaPuntosObjetos;
        this.verbose = verbose;
        this.timeStep = timeStep;
        this.rsp = new IncrementalSweepAndPrune(listaObjetos, listaVelocidades);
        this.listaVelocidades = listaVelocidades;
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        this.rspStats = new StatisticsIncrementalSP(listaPuntosObjetos.size());
    }

    /**
     *
     */
    @Override
    public void exec() {
        // recordar que el timeStep esta en FPS
        int frameMax = (int) ((this.tfinal - this.tinicial) * this.timeStep);
        this.rsp.initializeStructures();
        //this.rsp.setSpeedList(listaVelocidades);

        for (int i = 0; i <= frameMax; i++) {
            double t = (double) i / this.timeStep;

            // primer frame
            if (i == 0) {
                List<Pair<Integer>> listaPares = this.rsp.getCollisionsList();
                if (listaPares != null) {
                    listaColisionesActual = new ArrayList<>(listaPares);
                } else {
                    listaColisionesActual = new ArrayList<>();
                }

                this.rspStats.addStatistics(t, listaColisionesActual);

                if (this.verbose) {
                    out.println("t=" + (this.tinicial + t));
                    out.println(listaColisionesActual.toString());
                }
            } else {
                // muevo los intervalos
                this.rsp.updatePositions(1.0 / this.timeStep);
                // verifico que el orden de los puntos no haya cambiado
                if (!this.rsp.validateEndpointsLists()) {
                    listaColisionesAnterior = new ArrayList<>(listaColisionesActual);
                    this.rsp.insertionSort();
                    listaColisionesActual = new ArrayList<>(this.rsp.getCollisionsList());
                    this.rspStats.addStatistics(t, new ArrayList<>(listaColisionesActual));

                    if (this.verbose) {
                        out.println("t=" + (this.tinicial + t));
                        out.println(listaColisionesActual.toString());
                    }
                }
            }
        }
    }

    /**
     *
     * @param filePath
     * @param fileName
     */
    @Override
    public void writeStatisticsToFile(String filePath, String fileName) {

    }

    /**
     *
     * @return
     */
    @Override
    public Statistics getStatistics() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + (this.verbose ? 1 : 0);
        hash = 37 * hash + Objects.hashCode(this.rsp);
        hash = 37 * hash + this.timeStep;
        hash = 37 * hash + Objects.hashCode(this.listaObjetos);
        hash = 37 * hash + Objects.hashCode(this.listaVelocidades);
        hash = 37 * hash + (int) (doubleToLongBits(this.tinicial) ^ (doubleToLongBits(this.tinicial) >>> 32));
        hash = 37 * hash + (int) (doubleToLongBits(this.tfinal) ^ (doubleToLongBits(this.tfinal) >>> 32));
        hash = 37 * hash + Objects.hashCode(this.rspStats);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final SimulationStaticIncrementalSweepAndPrune other = (SimulationStaticIncrementalSweepAndPrune) obj;
        if (this.verbose != other.verbose) {
            return false;
        }
        if (this.timeStep != other.timeStep) {
            return false;
        }
        if (doubleToLongBits(this.tinicial) != doubleToLongBits(other.tinicial)) {
            return false;
        }
        if (doubleToLongBits(this.tfinal) != doubleToLongBits(other.tfinal)) {
            return false;
        }
        if (!Objects.equals(this.rsp, other.rsp)) {
            return false;
        }
        if (!Objects.equals(this.listaObjetos, other.listaObjetos)) {
            return false;
        }
        if (!Objects.equals(this.listaVelocidades, other.listaVelocidades)) {
            return false;
        }
        return Objects.equals(this.rspStats, other.rspStats);
    }

    /**
     *
     * @return
     */
    public IncrementalSweepAndPrune getRsp() {
        return this.rsp;
    }

    /**
     *
     * @param rsp
     */
    public void setRsp(IncrementalSweepAndPrune rsp) {
        this.rsp = rsp;
    }

    /**
     *
     * @return
     */
    public List<List<Point2D>> getObjectsList() {
        return this.listaObjetos;
    }

    /**
     *
     * @param listaObjetos
     */
    public void setObjectsList(List<List<Point2D>> listaObjetos) {
        this.listaObjetos = listaObjetos;
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getSpeedList() {
        return this.listaVelocidades;
    }

    /**
     *
     * @return
     */
    public int getTimeStep() {
        return this.timeStep;
    }

}
