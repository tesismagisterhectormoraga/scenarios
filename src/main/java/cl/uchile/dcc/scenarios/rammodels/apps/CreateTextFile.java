/*
 * Based on Example of Java SE 8 for programmers of Paul & Harvey Deitel. 
 * Ch. 15.4 Sequencial-Access Text Files 
 */
package cl.uchile.dcc.scenarios.rammodels.apps;

import java.io.FileNotFoundException;
import static java.lang.System.err;
import static java.lang.System.exit;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;

/**
 *
 * @author hmoraga
 */
public class CreateTextFile {

    private Formatter output; // output text to a file
    private String fileName, texto;

    // constructor privado que elimina la posibilidad de llamar directamente a
    // construir mas de un objeto

    /**
     *
     * @param fileName
     * @param texto
     */
    public CreateTextFile(String fileName, String texto) {
        this.fileName = fileName;
        this.texto = texto;
    }

    /**
     *
     * @return
     */
    public String getFileName() {
        return this.fileName;
    }

    /**
     *
     * @param fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     *
     * @return
     */
    public String getTexto() {
        return this.texto;
    }

    /**
     *
     * @param texto
     */
    public void setTexto(String texto) {
        this.texto = texto;
    }

    /**
     *
     */
    public void create() {
        this.openFile(this.fileName);
        this.addRecords(this.texto);
        this.closeFile();
    }

    // open file
    private void openFile(String filename) {
        try {
            this.output = new Formatter(filename);
        } catch (SecurityException securityException) {
            err.println("Write permission denied. Terminating.");
            exit(1);
        } catch (FileNotFoundException fileNotFoundException) {
            err.println("Error opening file. Terminating.");
            exit(1);
        }
    }

    private void addRecords(String record) {
        try {
            this.output.format("%s", record);
        } catch (FormatterClosedException formatterClosedException) {
            err.println("Error writing to file. Terminating.");
        } catch (NoSuchElementException noSuchElementException) {
            err.println("Invalid input.");
        }
    }

    private void closeFile() {
        if (this.output != null) {
            this.output.close();
        }
    }
}
