
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.scenarios.rammodels.testscene;

import cl.uchile.dcc.scenarios.rammodels.apps.Simulation;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationKinetic;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationStaticDirectSweepAndPrune;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationStaticIncrementalSweepAndPrune;
import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class TestTresObjetos {

    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new PairDouble(-10.0, 10.0));
        dimensiones.add(new PairDouble(-10.0, 10.0));

        List<Point2D> listaPuntos = new ArrayList<>(4);
        List<List<Point2D>> listaObjetos = new ArrayList<>();
        List<Pair<Double>> listaVelocidades = new ArrayList<>();
        double areaTotalObjetos = 0;

        //int timeStep = Integer.parseInt(args[0]);  // en FPS
        // solo para debug
        int timeStep = 10; // en FPS
        assert (timeStep > 0);

        double tiempoSimulacion = 10.0;
        assert (tiempoSimulacion >= timeStep);
        String dirName = "D:\\experimentos\\tresObjetos-"+timeStep;

        // area inicial
        double areaSimulacion = (dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst()) * (dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst());

        // velocidades (por segundo)
        listaVelocidades.add(new PairDouble(1.0, -1.0)); // primer objeto
        listaVelocidades.add(new PairDouble(-2.0, 0.0)); // segundo objeto
        listaVelocidades.add(new PairDouble(-1.0, 2.0)); // tercer objeto

        // puntos del primer objeto
        listaPuntos.add(new Point2D(-3.0, 1.0));
        listaPuntos.add(new Point2D(-1.0, 1.5));
        listaPuntos.add(new Point2D(-5.0, 2.0));
        listaPuntos.add(new Point2D(-5.0, 1.5));

        // se agrega a la lista de poligonos y objetos
        Polygon2D polyAuxiliar = new AnyPolygon2D(listaPuntos, false);
        areaTotalObjetos += (polyAuxiliar.getArea());
        listaObjetos.add(listaPuntos);

        // segundo objeto
        listaPuntos = new ArrayList<>(3);
        listaPuntos.add(new Point2D(3.0, -2.0));
        listaPuntos.add(new Point2D(2.0, 2.0));
        listaPuntos.add(new Point2D(1.0, -2.0));

        // se agrega a la lista de objetos
        polyAuxiliar = new AnyPolygon2D(listaPuntos, false);
        areaTotalObjetos += (polyAuxiliar.getArea());
        listaObjetos.add(listaPuntos);

        // tercer objeto
        listaPuntos = new ArrayList<>(4);
        listaPuntos.add(new Point2D(-3.0, -4.0));
        listaPuntos.add(new Point2D(-1.0, -3.0));
        listaPuntos.add(new Point2D(-2.0, -2.0));
        listaPuntos.add(new Point2D(-3.0, -2.0));

        // genero la lista de poligonos
        polyAuxiliar = new AnyPolygon2D(listaPuntos, false);
        areaTotalObjetos += (polyAuxiliar.getArea());
        listaObjetos.add(listaPuntos);
        
        // genero el archivo asociado
        File theDir = new File(dirName);

        if (!theDir.exists()) {
            out.println("creating directory: " + dirName);
            theDir.mkdirs();
            out.println("DIRECTORIES created");
        }

        out.println("Generando experimento...");

        out.println("Area del frame original:" + areaSimulacion);
        out.println("Area de los poligonos iniciales:" + areaTotalObjetos);

        // genero el archivo original único
        // se define el marco de la simulacion
        // los tiempos de la simulacion y los objetos
        // cada objeto esta definidos por dos lineas
        // la primera dice la velocidad del objeto
        // la segunda el poligono, dado como
        // numero_puntos x0 y0 x1 y1 ... xn yn
        //patternString = "marco:\\(\\((-?\\d+?.\\d+),(-?\\d+?.\\d+)\\),\\((-?\\d+?.\\d+),(-?\\d+?.\\d+)\\)\\)";
        //patternString = "timeIntervals:(\\d+.?\\d+)-(\\d+.?\\d+)";
        //patternString = "framesPerSecond:(\\d+)";
        //patternString = "(-?\\d+?.\\d+)\\s+(-?\\d+?.\\d+)"; <--- velocidades
        //patternString = "(\\d+)\\s+(-?\\d+?.\\d+)\\s+(-?\\d+?.\\d+)... ; <--- puntos del objeto

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(dirName+"\\inicio.txt", true))) {
            bw.write("marco:(" + dimensiones.get(0).getFirst() + "," + dimensiones.get(0).getSecond() + "),(" + dimensiones.get(1).getFirst() + "," + dimensiones.get(1).getSecond() + ")");
            bw.newLine();
            bw.write("timeInterval:0.0-" + tiempoSimulacion);
            bw.newLine();
            bw.write("framesPerSecond:" + timeStep);
            bw.newLine();
            for (int i = 0; i < listaObjetos.size(); i++) {
                Pair<Double> velocidades = listaVelocidades.get(i);
                bw.write(velocidades.getFirst() + " " + velocidades.getSecond());
                bw.newLine();

                List<Point2D> listaPuntosTmp = listaObjetos.get(i);
                StringBuilder pointsLine = new StringBuilder();
                pointsLine.append(listaPuntosTmp.size()).append(" ");
                listaPuntosTmp.stream().forEach((p) -> {
                    pointsLine.append(p.getCoords(0)).append(" ").append(p.getCoords(1)).append(" ");
                });
                String str = pointsLine.substring(0, pointsLine.length() - 1);
                bw.write(str);
                bw.newLine();
            }
        } catch (IOException ex) {
            out.println("Some error here:" + ex.getMessage());
        }        

        out.println("Simulacion Swep and Prune Directo");
        Simulation simulaSPDirecto = new SimulationStaticDirectSweepAndPrune(listaObjetos, dimensiones, listaVelocidades, timeStep, 0.0, tiempoSimulacion, true);
        simulaSPDirecto.exec();

        // simulacion del Sweep and prune Incremental
        out.println("Simulacion Swep and Prune Incremental");
        Simulation simulaSPIncremental = new SimulationStaticIncrementalSweepAndPrune(listaObjetos, listaVelocidades, timeStep, 0.0, tiempoSimulacion, true);
        simulaSPIncremental.exec();

        // KSP
        out.println("Simulacion Swep and Prune Cinético");
        Simulation simula = new SimulationKinetic(listaObjetos, listaVelocidades, 0.0, tiempoSimulacion, dimensiones, true);
        simula.exec();
    }
}
