package cl.uchile.dcc.scenarios.rammodels.apps;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import static cl.uchile.dcc.staticlibrary.poligons.PolygonFactory.getPolygon;
import cl.uchile.dcc.staticlibrary.poligons.PolygonType;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.cos;
import static java.lang.Math.max;
import static java.lang.Math.sin;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Clase util solo para los objetos cineticos
 *
 * @author hmoraga
 */
public class ObjectRandomGenerator {

    private List<Point2D> listaPuntos;
    private final Pair<Double> velocidades;

    /**
     *
     * @param dimensiones dimensiones en largo y ancho
     * @param percentOfTotalArea porcentaje del area total
     * @param timeInterval intervalo de tiempo (partiendo de 0.0)
     * @param rnd
     */
    public ObjectRandomGenerator(List<Pair<Double>> dimensiones,
            double percentOfTotalArea, double timeInterval, Random rnd) {
        this.listaPuntos = new ArrayList<>();

        double dimX = dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst();
        double dimY = dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst();
        double areaTotal = dimX * dimY;

        // obtencion de un polygono regular al azar
        PolygonType polyType = PolygonType.values()[rnd.nextInt(PolygonType.values().length)];
        //System.out.println("poligono elegido: " + polyType.toString());

        // area Individual de cada objeto
        double areaIndividual = areaTotal * percentOfTotalArea;

        // creo el objeto
        Polygon2D poli = getPolygon(polyType, areaIndividual, true);
        //if (poli.getVertices().get(0).getX() == Double.NaN){
        //    System.out.println("polgono invalido");
        //}

        double posX = this.obtenerAzarPorRango(dimensiones.get(0).getFirst(), dimensiones.get(0).getSecond(), rnd);
        double posY = this.obtenerAzarPorRango(dimensiones.get(1).getFirst(), dimensiones.get(1).getSecond(), rnd);

        // calculo el min y max de cada coordenada
        double minX = poli.getBox().getMinX();
        double maxX = poli.getBox().getMaxX();
        double minY = poli.getBox().getMinY();
        double maxY = poli.getBox().getMaxY();

        if (posX + minX < dimensiones.get(0).getFirst()) {
            posX = dimensiones.get(0).getFirst() - minX;
        } else if (posX + maxX > dimensiones.get(0).getSecond()) {
            posX = dimensiones.get(0).getSecond() - maxX;
        }

        if (posY + minY < dimensiones.get(1).getFirst()) {
            posY = dimensiones.get(1).getFirst() - minY;
        } else if (posY + maxY > dimensiones.get(1).getSecond()) {
            posY = dimensiones.get(1).getSecond() - maxY;
        }

        poli.desplazar(posX, posY);

        this.listaPuntos = poli.getVertices();

        // obtencion de su velocidad al azar
        // se elije la mayor diferencia entre anchos en X y en Y
        double ancho = abs(poli.getBox().getMaxX() - poli.getBox().getMinX());
        double alto = abs(poli.getBox().getMaxY() - poli.getBox().getMinY());
        double valor = this.obtenerAzarPorRango(-3.5 * max(ancho, alto), 3.5 * max(ancho, alto), rnd); // 3.5 veces el ancho del objeto (por segundo)
        double anguloAzar = this.obtenerAzarPorRango(0, 2 * PI, rnd);
        double vx = valor * cos(anguloAzar);
        double vy = valor * sin(anguloAzar);
        // se crea el vector velocidad
        this.velocidades = new PairDouble(vx, vy);
    }

    /**
     *
     * @param dimensiones dimensiones en largo y ancho
     * @param percentOfTotalArea porcentaje del area total
     * @param objectSpeedRanges par de valores de velocidades minimo y maximo
     * como porcentaje del radio de un objeto
     * @param timeInterval intervalo de tiempo (partiendo de 0.0)
     * @param rnd
     */
    public ObjectRandomGenerator(List<Pair<Double>> dimensiones,
            double percentOfTotalArea, Pair<Double> objectSpeedRanges,
            double timeInterval, Random rnd) {
        this.listaPuntos = new ArrayList<>();

        double dimX = dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst();
        double dimY = dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst();
        double areaTotal = dimX * dimY;

        // obtencion de un polygono regular al azar
        PolygonType polyType = PolygonType.values()[rnd.nextInt(PolygonType.values().length)];
        //System.out.println("poligono elegido: " + polyType.toString());

        // area Individual de cada objeto
        double areaIndividual = areaTotal * percentOfTotalArea;

        // creo el objeto
        Polygon2D poli = getPolygon(polyType, areaIndividual, true);
        //if (poli.getVertices().get(0).getX() == Double.NaN){
        //    System.out.println("polgono invalido");
        //}

        double posX = this.obtenerAzarPorRango(dimensiones.get(0).getFirst(), dimensiones.get(0).getSecond(), rnd);
        double posY = this.obtenerAzarPorRango(dimensiones.get(1).getFirst(), dimensiones.get(1).getSecond(), rnd);

        // calculo el min y max de cada coordenada
        double minX = poli.getBox().getMinX();
        double maxX = poli.getBox().getMaxX();
        double minY = poli.getBox().getMinY();
        double maxY = poli.getBox().getMaxY();

        if (posX + minX < dimensiones.get(0).getFirst()) {
            posX = dimensiones.get(0).getFirst() - minX;
        } else if (posX + maxX > dimensiones.get(0).getSecond()) {
            posX = dimensiones.get(0).getSecond() - maxX;
        }

        if (posY + minY < dimensiones.get(1).getFirst()) {
            posY = dimensiones.get(1).getFirst() - minY;
        } else if (posY + maxY > dimensiones.get(1).getSecond()) {
            posY = dimensiones.get(1).getSecond() - maxY;
        }

        poli.desplazar(posX, posY);

        this.listaPuntos = poli.getVertices();

        // obtencion de su velocidad al azar
        // se elije la mayor diferencia entre anchos en X y en Y
        double ancho = abs(poli.getBox().getMaxX() - poli.getBox().getMinX());
        double alto = abs(poli.getBox().getMaxY() - poli.getBox().getMinY());
        double valor = max(ancho, alto) * this.obtenerAzarPorRango(objectSpeedRanges.getFirst(), objectSpeedRanges.getSecond(), rnd);
        double anguloAzar = this.obtenerAzarPorRango(0, 2 * PI, rnd);
        double vx = valor * cos(anguloAzar);
        double vy = valor * sin(anguloAzar);
        // se crea el vector velocidad
        this.velocidades = new PairDouble(vx, vy);
    }

    /**
     *
     * @return
     */
    public List<Point2D> getListaPuntos() {
        return this.listaPuntos;
    }

    /**
     *
     * @return
     */
    public Pair<Double> getVelocidades() {
        return this.velocidades;
    }

    private double obtenerAzarPorRango(double minimo, double maximo, Random rnd) {
        return rnd.nextDouble() * (maximo - minimo) + minimo;
    }
}
