/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.scenarios.rammodels.testscene;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.parseDouble;
import static java.lang.Integer.parseInt;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import cl.uchile.dcc.scenarios.rammodels.apps.Simulation;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationStaticIncrementalSweepAndPrune;
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import cl.uchile.dcc.staticlibrary.poligons.Triangle;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;

/**
 *
 * @author hmoraga
 */
public class TestEquidistribucion {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        int cantObjetosPorFila = parseInt(args[0]);
        int cantObjetosPorColumna = cantObjetosPorFila;
        // SOLO por DEBUG
        //int cantObjetosPorFila = 30, cantObjetosPorColumna = 30;
        int FPS = parseInt(args[1]);
        // SOLO por DEBUG
        //int FPS = 100;

        double largeSize = 10.0;
        List<List<Point2D>> listaPuntosObjetos = new ArrayList<>(cantObjetosPorFila * cantObjetosPorColumna);
        List<Pair<Double>> velocidadesPorObjeto = new ArrayList<>(cantObjetosPorFila * cantObjetosPorColumna);

        // Se elige un marco de simulacion
        // valores posibles (-3.0,3.0)-(-5.0,5.0)-(-10.0,10.0)-(-20.0,20.0)-(-50.0,50.0)
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new PairDouble(-largeSize, largeSize));
        dimensiones.add(new PairDouble(-largeSize, largeSize));

        // Intervalo de tiempo
        Pair<Double> tiempos = new PairDouble(0.0, parseDouble(args[2]));
        // SOLO POR DEBUG
        //Pair<Double> tiempos = new Pair<>(0.0, 1.0);

        // calculamos el tamaño de cada objeto, por simplicidad será
        // un grupo de triángulos
        double tamanoObj = 2 * largeSize / (3 * cantObjetosPorFila);
        Random rnd = new Random(currentTimeMillis());
        double posX = largeSize / cantObjetosPorFila;
        double posY = largeSize / cantObjetosPorColumna;

        for (int i = 0; i < cantObjetosPorFila; i++) {
            for (int j = 0; j < cantObjetosPorColumna; j++) {
                Polygon2D poly = new Triangle(true);
                poly.amplificar(tamanoObj);
                poly.desplazar(2 * i * largeSize / cantObjetosPorFila + posX - largeSize, 2 * j * largeSize / cantObjetosPorColumna + posY - largeSize);
                velocidadesPorObjeto.add(obtainRandomSpeeds(rnd, 3 * tamanoObj, 3 * tamanoObj));
                listaPuntosObjetos.add(poly.getVertices());
            }
        }

        // genero el archivo para que sea de utilidad en el 
        // proyecto ShowModel
        File theDir = new File("D:\\experimentos\\equidistribucion");

        if (!theDir.exists()) {
            out.println("Creating directory: " + theDir.getName());
            boolean result = false;

            try {
                result = theDir.mkdirs();
            } catch (SecurityException e) {
            }

            if (result) {
                out.println("Directorios " + theDir.getName() + " creado");
            }
        }

        File arch = new File(theDir.toString() + "\\input.txt");

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(arch))) {
            bw.write("marco:" + dimensiones.get(0).toString() + "," + dimensiones.get(1).toString());
            bw.newLine();
            bw.write("timeIntervals:" + tiempos.getFirst() + "-" + tiempos.getSecond());
            bw.newLine();
            bw.write("framesPerSecond:" + FPS);
            bw.newLine();

            for (int i = 0; i < listaPuntosObjetos.size(); i++) {
                List<Point2D> obj = listaPuntosObjetos.get(i);

                bw.write(velocidadesPorObjeto.get(i).getFirst() + " " + velocidadesPorObjeto.get(i).getSecond());
                bw.newLine();
                StringBuilder str = new StringBuilder("" + obj.size());

                for (int j = 0; j < obj.size(); j++) {
                    str.append(" ").append(obj.get(j).getX()).append(" ").append(obj.get(j).getY());
                }
                bw.write(str.toString());
                bw.newLine();
            }
        } catch (IOException e) {
        }

        Simulation simulaRealSP = new SimulationStaticIncrementalSweepAndPrune(listaPuntosObjetos, velocidadesPorObjeto, FPS, tiempos.getFirst(), tiempos.getSecond(), false);
        simulaRealSP.exec();
    }

    private static Pair<Double> obtainRandomSpeeds(Random rnd, double rangoX, double rangoY) {
        double speedX = rnd.nextDouble() * 2 * rangoX - rangoX;
        double speedY = rnd.nextDouble() * 2 * rangoY - rangoY;

        return new PairDouble(speedX, speedY);
    }
}
