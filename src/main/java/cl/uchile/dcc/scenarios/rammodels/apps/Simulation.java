package cl.uchile.dcc.scenarios.rammodels.apps;

import cl.uchile.dcc.staticlibrary.sap.Statistics;
import java.io.IOException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Clase que genera simulacion
 *
 * @author hmoraga
 */
public interface Simulation {

    /**
     *
     */
    void exec();

    /**
     *
     * @param filePath
     * @param fileName
     * @throws IOException
     */
    void writeStatisticsToFile(String filePath, String fileName) throws IOException;

    /**
     *
     * @return
     */
    Statistics getStatistics();
}
