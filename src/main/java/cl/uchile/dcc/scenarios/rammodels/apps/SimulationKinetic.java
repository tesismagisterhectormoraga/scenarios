package cl.uchile.dcc.scenarios.rammodels.apps;

import cl.uchile.dcc.kineticlibrary.kinetic.IntersectionsList;
import cl.uchile.dcc.kineticlibrary.kinetic.KSLList;
import cl.uchile.dcc.kineticlibrary.kinetic.KineticSortedList;
import cl.uchile.dcc.kineticlibrary.kinetic.Object2D;
import cl.uchile.dcc.kineticlibrary.kinetic.Wrapper;
import cl.uchile.dcc.kineticlibrary.kinetic.actuators.KineticStatistics;
import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsObserver;
import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsQueue;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.sap.Statistics;
import java.io.File;
import java.io.IOException;
import static java.lang.Double.doubleToLongBits;
import static java.lang.System.out;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Observable;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;

/**
 *
 * @author hmoraga
 */
public class SimulationKinetic implements Simulation {
    private List<Object2D> listaObjetos;
    private final double tinicial, tfinal;
    private Statistics estadisticas;
    private final boolean verbose;

    /**
     * método para simulación de modelo cinético
     *
     * @param listaPuntosObjetos
     * @param listaVelocidades
     * @param tinicial tiempo inicial
     * @param tfinal tiempo final
     * @param dimensiones dimesnsiones [a,b]x[c,d]
     * @param verbose
     */
    public SimulationKinetic(List<List<Point2D>> listaPuntosObjetos, List<Pair<Double>> listaVelocidades, double tinicial, double tfinal,
            List<Pair<Double>> dimensiones, boolean verbose) {
        assert(listaPuntosObjetos.size() == listaVelocidades.size());
        
        this.listaObjetos = new ArrayList<>(listaPuntosObjetos.size());
        
        for (int i = 0; i < listaPuntosObjetos.size(); i++) {
            List<Point2D> obj = listaPuntosObjetos.get(i);
            Pair<Double> veloc = listaVelocidades.get(i);
            this.listaObjetos.add(new Object2D(i, obj, veloc, tinicial));
        }
        
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        double areaTotal = (dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst()) * (dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst());
        this.estadisticas = new KineticStatistics();
        this.estadisticas.setTotalArea(areaTotal);

        double areaInicial = 0.0;
        for (int i = 0; i < this.listaObjetos.size(); i++) {
            Object2D obj = this.listaObjetos.get(i);
            areaInicial += obj.getArea();
        }
        this.estadisticas.setInitialOcupatedArea(areaInicial);
        this.estadisticas.setSimulationInterval(new PairDouble(tinicial, tfinal));
        this.verbose = verbose;
    }

    /**
     *
     */
    @Override
    public void exec() {
        KSLList listaKSL = new KSLList(2);
        // inicializo la lista de colisiones
        IntersectionsList colisiones = new IntersectionsList();
        EventsQueue colaEventos = new EventsQueue(new PairDouble(this.tinicial, this.tfinal), 2 * this.listaObjetos.size(), this.verbose);

        // inicializo el observer
        EventsObserver observador = new EventsObserver(listaKSL, colisiones, colaEventos, (KineticStatistics) this.estadisticas, this.verbose);

        // agrego el observer a todos los objetos necesarios
        listaKSL.setObserver(observador);
        colisiones.setObserver(observador);
        colaEventos.setObserver(observador);
        ((KineticStatistics) this.estadisticas).setObserver(observador);

        // obtengo la cola de Eventos inicializada
        this.initializeStructures(colaEventos, listaKSL, colisiones, this.verbose);

        // procesar todos los eventos hasta que la cola de eventos esté vacia
        colaEventos.processEvents();

        // limpiar todo
        ((Observable) this.estadisticas).deleteObservers();
        colaEventos.deleteObservers();
        colisiones.deleteObservers();
        listaKSL.deleteObservers();

        // limpiar todos los objetos del tipo Collection
        colisiones.clear();
        colaEventos.clear();
        listaKSL.clear();
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.listaObjetos);
        hash = 43 * hash + (int) (doubleToLongBits(this.tinicial) ^ (doubleToLongBits(this.tinicial) >>> 32));
        hash = 43 * hash + (int) (doubleToLongBits(this.tfinal) ^ (doubleToLongBits(this.tfinal) >>> 32));
        hash = 43 * hash + (this.verbose ? 1 : 0);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final SimulationKinetic other = (SimulationKinetic) obj;
        if (doubleToLongBits(this.tinicial) != doubleToLongBits(other.tinicial)) {
            return false;
        }
        if (doubleToLongBits(this.tfinal) != doubleToLongBits(other.tfinal)) {
            return false;
        }
        if (this.verbose != other.verbose) {
            return false;
        }
        return Objects.equals(this.listaObjetos, other.listaObjetos);
    }
    
    /**
     *
     * @param filePath
     * @param fileName
     */
    @Override
    public void writeStatisticsToFile(String filePath, String fileName) {
        try {
            String dirName = filePath;
            File theDir = new File(dirName);

            if (!theDir.exists()) {
                if (this.verbose) {
                    out.println("creating directory: " + dirName);
                }
                theDir.mkdirs();
                if (this.verbose) {
                    out.println("DIRECTORIES created");
                }
            }

            Path arch = get(dirName + "\\" + fileName);
            this.estadisticas.writeToFile(arch);            
        } catch (IOException ex) {
            getLogger(SimulationKinetic.class.getName()).log(SEVERE, null, ex);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Statistics getStatistics() {
        return this.estadisticas;
    }

    /**
     *
     * @param colaEventos
     * @param listaKSL
     * @param colisiones
     * @param verbose
     */
    public void initializeStructures(EventsQueue colaEventos, KSLList listaKSL, IntersectionsList colisiones,
            boolean verbose) {
        // se supone que tengo 2 dimensiones, por lo que debo crear dos KSL temporales
        // genero una KSL por cada dimension
        KineticSortedList KSLx = new KineticSortedList(), KSLy = new KineticSortedList();

        // de todos los objetos debo generar los DimensionKinetic
        this.listaObjetos.stream().forEach((listaObjeto) -> {
            List<Wrapper> wrappersX = listaObjeto.getWrappers(0);
            List<Wrapper> wrappersY = listaObjeto.getWrappers(1);
            // de cada DimensionKinetic genero su Wrapper
            // y agrego los Wrapper a cada KSL
            KSLx.addAll(wrappersX);
            KSLy.addAll(wrappersY);
        });

        // ingreso los valores en la lista SIN ORDENAR
        // seran ordenados al calcular los eventos iniciales
        listaKSL.add(KSLx);
        listaKSL.add(KSLy);

        // calculo los eventos iniciales
        colaEventos.calculateInitialEvents();
        
        if (verbose){
            for (int i = 0; i < listaKSL.size(); i++) {
                KineticSortedList ksl = listaKSL.get(i);
                out.println(ksl.toString());
            }
            
            out.println("");            
        }
    }
    
    /**
     *
     * @return
     */
    public List<Object2D> getObjetsList() {
        return this.listaObjetos;
    } 

    /**
     *
     * @return
     */
    public double getTinicial() {
        return this.tinicial;
    }

    /**
     *
     * @return
     */
    public double getTfinal() {
        return this.tfinal;
    }

    /**
     *
     * @return
     */
    public boolean isVerbose() {
        return this.verbose;
    }
}
