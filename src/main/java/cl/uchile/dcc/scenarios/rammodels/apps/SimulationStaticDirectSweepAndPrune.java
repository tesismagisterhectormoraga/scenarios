package cl.uchile.dcc.scenarios.rammodels.apps;

import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import cl.uchile.dcc.staticlibrary.sap.DirectSweepAndPrune2D;
import cl.uchile.dcc.staticlibrary.sap.Statistics;
import cl.uchile.dcc.staticlibrary.sap.StatisticsDirectSP;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Double.doubleToLongBits;
import static java.lang.System.out;
import java.util.List;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hmoraga
 */
public class SimulationStaticDirectSweepAndPrune implements Simulation {

    private final boolean verbose;
    private final int timeStep;
    private final int totalFrames;
    private final double tinicial, tfinal;
    private List<List<Point2D>> listaPuntosInicialObjetos;
    private final List<Pair<Double>> dimensiones, listaVelocidades;
    private StatisticsDirectSP estadisticas;
    private DirectSweepAndPrune2D swp;

    /**
     *
     * @param listaPuntosObjetos
     * @param dimensiones
     * @param listaVelocidades
     * @param timeStep
     * @param tinicial
     * @param tfinal
     * @param verbose
     */
    public SimulationStaticDirectSweepAndPrune(List<List<Point2D>> listaPuntosObjetos, List<Pair<Double>> dimensiones, List<Pair<Double>> listaVelocidades,
            int timeStep, double tinicial, double tfinal, boolean verbose) {
        this.dimensiones = dimensiones;
        this.tinicial = tinicial;
        this.tfinal = tfinal;
        this.timeStep = timeStep;
        this.listaPuntosInicialObjetos = listaPuntosObjetos;
        this.listaVelocidades = listaVelocidades;
        this.totalFrames = (int) ((this.tfinal - this.tinicial) * this.timeStep);

        double areaTotal = (dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst()) * (dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst());
        this.verbose = verbose;
        double area = this.calculateObjectsArea(listaPuntosObjetos);

        this.estadisticas = new StatisticsDirectSP(listaPuntosObjetos.size(), new PairDouble(tinicial, tfinal));
        this.estadisticas.setTotalArea(areaTotal);
        this.estadisticas.setInitialOcupatedArea(area);
    }

    /**
     *
     */
    @Override
    public void exec() {
        List<Pair<Integer>> listaColisiones;
        this.swp = new DirectSweepAndPrune2D(listaPuntosInicialObjetos);

        for (int frameNumber = 0; frameNumber <= this.totalFrames; frameNumber++) {
            double t = (double) frameNumber / this.timeStep;
            if (this.verbose) {
                out.println("t=" + t);
            }

            // primer frame
            if (frameNumber == 0) {
                this.swp.initializeStructures(this.listaPuntosInicialObjetos);
                this.swp.setSpeedList(this.listaVelocidades);
                listaColisiones = this.swp.getPairsListColliding();
            } else {
                // muevo las cajas
                this.swp.updateBoxesPositions(1.0 / this.timeStep);

                this.swp.insertionSort(); // reordeno la lista de cajas
                listaColisiones = this.swp.getPairsListColliding();
            }
            if (this.verbose) {
                out.println(listaColisiones.toString());
            }
        }
    }

    /**
     *
     * @param filePath
     * @param fileName
     * @throws IOException
     */
    @Override
    public void writeStatisticsToFile(String filePath, String fileName) throws IOException {
        File theDir = new File(filePath);

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try {
                theDir.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                out.println("DIR created");
            }
        }

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath + "\\" + fileName))) {
            for (String s : this.estadisticas.getText()) {
                bw.append(s);
                bw.newLine();
            }
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Statistics getStatistics() {
        return this.estadisticas;
    }

    private double calculateObjectsArea(List<List<Point2D>> listaPuntosObjetos) {
        double area = 0.0;

        area = listaPuntosObjetos.stream().map((listaPuntosObjeto) -> new AnyPolygon2D(listaPuntosObjeto, false).getArea()).reduce(area, (accumulator, _item) -> accumulator + _item);

        return area;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (this.verbose ? 1 : 0);
        hash = 47 * hash + this.timeStep;
        hash = 47 * hash + this.totalFrames;
        hash = 47 * hash + (int) (doubleToLongBits(this.tinicial) ^ (doubleToLongBits(this.tinicial) >>> 32));
        hash = 47 * hash + (int) (doubleToLongBits(this.tfinal) ^ (doubleToLongBits(this.tfinal) >>> 32));
        hash = 47 * hash + Objects.hashCode(this.listaPuntosInicialObjetos);
        hash = 47 * hash + Objects.hashCode(this.dimensiones);
        hash = 47 * hash + Objects.hashCode(this.listaVelocidades);
        hash = 47 * hash + Objects.hashCode(this.swp);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final SimulationStaticDirectSweepAndPrune other = (SimulationStaticDirectSweepAndPrune) obj;
        if (this.verbose != other.verbose) {
            return false;
        }
        if (this.timeStep != other.timeStep) {
            return false;
        }
        if (this.totalFrames != other.totalFrames) {
            return false;
        }
        if (doubleToLongBits(this.tinicial) != doubleToLongBits(other.tinicial)) {
            return false;
        }
        if (doubleToLongBits(this.tfinal) != doubleToLongBits(other.tfinal)) {
            return false;
        }
        if (!Objects.equals(this.listaPuntosInicialObjetos, other.listaPuntosInicialObjetos)) {
            return false;
        }
        if (!Objects.equals(this.dimensiones, other.dimensiones)) {
            return false;
        }
        if (!Objects.equals(this.listaVelocidades, other.listaVelocidades)) {
            return false;
        }
        return Objects.equals(this.swp, other.swp);
    }

    /**
     *
     * @return
     */
    public int getTimeStep() {
        return this.timeStep;
    }

    /**
     *
     * @return
     */
    public double getTinicial() {
        return this.tinicial;
    }

    /**
     *
     * @return
     */
    public double getTfinal() {
        return this.tfinal;
    }

    /**
     *
     * @return
     */
    public List<List<Point2D>> getInitialPointsListOfObjects() {
        return this.listaPuntosInicialObjetos;
    }

    /**
     *
     * @param listaPuntosInicialObjetos
     */
    public void setInitialPointsListOfObjects(List<List<Point2D>> listaPuntosInicialObjetos) {
        this.listaPuntosInicialObjetos = listaPuntosInicialObjetos;
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getDimensiones() {
        return this.dimensiones;
    }

    /**
     *
     * @return
     */
    public List<Pair<Double>> getSpeedList() {
        return this.listaVelocidades;
    }

}
