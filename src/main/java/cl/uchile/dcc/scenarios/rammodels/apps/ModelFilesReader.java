package cl.uchile.dcc.scenarios.rammodels.apps;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.IOException;
import static java.lang.System.exit;
import static java.lang.System.out;
import java.nio.charset.Charset;
import static java.nio.charset.Charset.forName;
import static java.nio.file.Files.readAllLines;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.util.regex.Pattern.compile;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Clase que lee el modelo desde un archivo de texto
 *
 * @author hmoraga
 */
public class ModelFilesReader {
    private final String filePath;    //filePath del directorio inicial a leer
    private final String type;    // tipo de metodo a ejecutar o crear
    private final boolean verbose;

    /**
     *
     * @param filePath dir del experimento a leer (la ruta completa)
     * @param simulationType tipo de simulacion: static, kinetic, bruteForce
     * @param verbose
     */
    public ModelFilesReader(String filePath, String simulationType, boolean verbose) {
        this.filePath = filePath;
        // inicializo objetos
        this.type = simulationType;
        this.verbose = verbose;
    }

    /**
     * Método me debe generar todas las estructuras necesarias para leer UN
     * archivo estático y luego el método exec me ejecutará el metodo
     * respectivo.
     *
     * @param fileName archivo estático a leer
     * @return 
     * @throws java.io.IOException si el archivo no se encuentra
     */
    public Simulation readFile(String fileName) throws IOException {
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        List<Pair<Double>> listaVelocidades;
        List<List<Point2D>> listaPuntosPorObjeto;
        Pair<Double> tiemposSimulacion;
        Integer fps;

        Pattern pattern;
        Matcher matcher;

        if (this.verbose) {
            out.println("Leyendo archivo " + fileName); // leo los datos del archivo
        }
        Charset utf8 = forName("UTF-8");
        List<String> lista = readAllLines(get(filePath), utf8);
        // linea 1: dimensiones
        pattern = compile("marco.*\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\),\\((-?\\d+.\\d+),(-?\\d+.\\d+)\\)");
        matcher = pattern.matcher(lista.get(0));

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            String grupo2 = matcher.group(2);
            String grupo3 = matcher.group(3);
            String grupo4 = matcher.group(4);

            dimensiones.add(new PairDouble(Double.valueOf(grupo1), Double.valueOf(grupo2))); // en X
            dimensiones.add(new PairDouble(Double.valueOf(grupo3), Double.valueOf(grupo4))); // en Y
        } else {
            dimensiones.add(new PairDouble(-10.0, 10.0)); // en X
            dimensiones.add(new PairDouble(-10.0, 10.0)); // en Y
        }
        matcher.reset();
        // linea 2: intervalo simulacion
        pattern = compile("timeIntervals.*(\\d+.\\d+)-(\\d+.\\d+)");
        matcher = pattern.matcher(lista.get(1));

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            String grupo2 = matcher.group(2);

            tiemposSimulacion = new PairDouble(Double.valueOf(grupo1), Double.valueOf(grupo2));
        } else {
            tiemposSimulacion = new PairDouble(0.0, 10.0);
        }
        matcher.reset();
        // linea 3: fps
        pattern = compile("framesPerSecond:(\\d+)");
        matcher = pattern.matcher(lista.get(2));

        if (matcher.find()) {
            String grupo1 = matcher.group(1);
            fps = Integer.valueOf(grupo1);
        } else {
            fps = 10;
        }
        matcher.reset();

        listaPuntosPorObjeto = new ArrayList<>((lista.size() - 3) / 2);
        listaVelocidades = new ArrayList<>((lista.size() - 3) / 2);

        // proceso los datos que se repiten
        for (int i = 3; i < lista.size(); i = i + 2) {
            Pair<Double> velocObjeto;

            // primer dato es las velocidades del objeto
            pattern = compile("^(-?\\d+.\\d+)\\s+(-?\\d+.\\d+)");
            matcher = pattern.matcher(lista.get(i));

            if (matcher.find()) {
                String grupo1 = matcher.group(1);
                String grupo2 = matcher.group(2);

                velocObjeto = new PairDouble(Double.valueOf(grupo1), Double.valueOf(grupo2));
            } else {
                velocObjeto = new PairDouble(0.0, 0.0);
            }
            listaVelocidades.add(velocObjeto);
            matcher.reset();

            // la otra linea
            // primer dato es la cantidad de puntos del objeto
            // el resto son las coordenadas de todos los puntos
            pattern = compile("^(\\d+)\\s+(.*)");
            matcher = pattern.matcher(lista.get(i + 1));

            if (matcher.find()) {
                int cantPuntos = Integer.valueOf(matcher.group(1));
                List<Point2D> listaPuntosObjeto = new ArrayList<>(cantPuntos);
                String[] aux = matcher.group(2).split("\\s+");

                for (int j = 0; j < cantPuntos; j++) {
                    double posX = Double.valueOf(aux[2 * j]);
                    double posY = Double.valueOf(aux[2 * j + 1]);

                    listaPuntosObjeto.add(new Point2D(posX, posY));
                }
                listaPuntosPorObjeto.add(listaPuntosObjeto);
            }
            matcher.reset();
        }

        switch (this.type) {
            case "direct":
                return new SimulationStaticDirectSweepAndPrune(listaPuntosPorObjeto, dimensiones, listaVelocidades, fps, tiemposSimulacion.getFirst(), tiemposSimulacion.getSecond(), this.verbose);
            case "incremental":
                return new SimulationStaticIncrementalSweepAndPrune(listaPuntosPorObjeto, listaVelocidades, fps, tiemposSimulacion.getFirst(), tiemposSimulacion.getSecond(), this.verbose);
            case "kinetic":
                return new SimulationKinetic(listaPuntosPorObjeto, listaVelocidades, tiemposSimulacion.getFirst(), tiemposSimulacion.getSecond(), dimensiones, this.verbose);
            default:
                try {
                    throw new IllegalArgumentException("Simulation Type unknown");
                } catch (IllegalArgumentException ex) {
                    exit(1);
                }
                break;
        }
        return null;
    }
}
