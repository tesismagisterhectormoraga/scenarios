/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.scenarios.rammodels.testscene;

import java.io.IOException;
import static java.lang.String.valueOf;
import static java.lang.System.currentTimeMillis;
import static java.nio.file.Files.createDirectories;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;
import cl.uchile.dcc.scenarios.rammodels.apps.ModelFilesReader;
import cl.uchile.dcc.scenarios.rammodels.apps.Simulation;

/**
 *
 * @author hmoraga
 */
public class TestReadFromFiles {
    public static void main(String[] args) {
        try {
            // solo para debug
            //Path filePath = Paths.get("D:\\experimentos\\1_PORCIENTO_1000FPS\\1024OBJ\\experimento1505580209382\\original.txt");
            Path filePath = get(args[0]);
            String directory = filePath.toString().substring(0,filePath.toString().lastIndexOf('\\'));
            String fileName = filePath.toString().substring(filePath.toString().lastIndexOf('\\')+1);
            ModelFilesReader mfr = new ModelFilesReader(filePath.toString(), "direct", false);
            Simulation simulacion = mfr.readFile(fileName);
            simulacion.exec();
            long timestamp = currentTimeMillis();
            Path newFilePath = get(directory, "nuevo", valueOf(timestamp));
            createDirectories(newFilePath);
            simulacion.writeStatisticsToFile(newFilePath.toString(), "\\direct-sp-statistics.txt");

            mfr = new ModelFilesReader(filePath.toString(), "incremental", false);
            simulacion = mfr.readFile(fileName);
            simulacion.exec();
            simulacion.writeStatisticsToFile(newFilePath.toString(), "\\incremental-sp-statistics.txt");

            mfr = new ModelFilesReader(filePath.toString(), "kinetic", false);
            simulacion = mfr.readFile(fileName);
            simulacion.exec();
            simulacion.writeStatisticsToFile(newFilePath.toString(), "\\ksp-statistics.txt");
        } catch (IOException ex) {
            getLogger(TestReadFromFiles.class.getName()).log(SEVERE, "{0}", ex);
        }
    }
}
