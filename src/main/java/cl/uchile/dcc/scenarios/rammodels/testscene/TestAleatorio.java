package cl.uchile.dcc.scenarios.rammodels.testscene;

import cl.uchile.dcc.kineticlibrary.kinetic.Object2D;
import cl.uchile.dcc.scenarios.rammodels.apps.Simulation;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationKinetic;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationStaticDirectSweepAndPrune;
import cl.uchile.dcc.scenarios.rammodels.apps.SimulationStaticIncrementalSweepAndPrune;
import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.String.valueOf;
import static java.lang.System.currentTimeMillis;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import cl.uchile.dcc.scenarios.rammodels.apps.ObjectRandomGenerator;

/**
 *
 * @author hmoraga
 */
public class TestAleatorio {

    /**
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        long timestamp = currentTimeMillis();
        Random rnd = new Random(timestamp);

        // area de la simulacion
        double areaSimulacion, areaTotalObjetos = 0;
        boolean verbose = false;

        // cantidad de objetos en la simulación
        // valores posibles: 0, 1, 2, 5, 10, 20, 30, 50, 100, 200, 500, 1000...
        //int cantObjetos = Integer.parseInt(args[0]);
        // SOLO PARA DEBUG
        int cantObjetos = 1000;

        List<List<Point2D>> listaPuntosObjetos = new ArrayList<>(cantObjetos);
        List<Object2D> listaObjetos = new ArrayList<>(cantObjetos);
        List<Pair<Double>> velocidadesPorObjeto = new ArrayList<>(cantObjetos);

        // Se elige un marco de simulacion
        // valores posibles (-3.0,3.0)-(-5.0,5.0)-(-10.0,10.0)-(-20.0,20.0)-(-50.0,50.0)
        List<Pair<Double>> dimensiones = new ArrayList<>(2);
        dimensiones.add(new PairDouble(-10.0, 10.0));
        dimensiones.add(new PairDouble(-10.0, 10.0));
        //areaSimulacion = (dimensiones.first.second-dimensiones.first.first)*(dimensiones.second.second-dimensiones.second.first);
        // tamaño máximo de un objeto (como porcentaje de las dimensiones)
        //double objectMaxSize = Double.parseDouble(args[1]) / cantObjetos;
        // SOLO PARA DEBUG
        double objectMaxSize = 0.1/cantObjetos;

        // Se elige un tiempo de simulacion: desde 0 al valor elegido
        // valores posibles 1.0, 2.0, 5.0, 10.0, 20.0
        double simulationTime = 1.0;

        // Se elige un timeStep (en FPS), util solo para las simulaciones estáticas
        //int timeStep = Integer.parseInt(args[2]);
        // SOLO PARA DEBUG
        int timeStep = 1000;

        String path = "D:\\experimentos\\experimento" + valueOf(timestamp);

        String dirName = path;
        File theDir = new File(dirName);

        if (!theDir.exists()) {
            if (verbose) {
                out.println("creating directory: " + dirName);
            }
            theDir.mkdirs();
            if (verbose) {
                out.println("DIRECTORIES created");
            }
        }

        if (verbose) {
            out.println("Generando experimento " + path);
        }

        for (int i = 0; i < cantObjetos; i++) {
            // generador de objetos al azar
            ObjectRandomGenerator org = new ObjectRandomGenerator(dimensiones, objectMaxSize, simulationTime, rnd);
            // agrego a la lista de velocidades
            velocidadesPorObjeto.add(org.getVelocidades());
            // agrego a la lista de puntos por objeto
            listaPuntosObjetos.add(org.getListaPuntos());
            listaObjetos.add(new Object2D(i, listaPuntosObjetos.get(i), velocidadesPorObjeto.get(i)));
        }

        areaTotalObjetos = listaPuntosObjetos.stream().map((objeto) -> new AnyPolygon2D(objeto, false).getArea()).reduce(areaTotalObjetos, (accumulator, _item) -> accumulator + _item);
        areaSimulacion = (dimensiones.get(0).getSecond() - dimensiones.get(0).getFirst()) * (dimensiones.get(1).getSecond() - dimensiones.get(1).getFirst());

        if (verbose) {
            out.println("Area del frame original:" + areaSimulacion);
            out.println("Area de los poligonos iniciales:" + areaTotalObjetos);
        }
        // genero el archivo original único
        // se define el marco de la simulacion
        // los tiempos de la simulacion y los objetos
        // cada objeto esta definidos por dos lineas
        // la primera dice la velocidad del objeto
        // la segunda el poligono, dado como
        // numero_puntos x0 y0 x1 y1 ... xn yn
        //patternString = "marco:\\(\\((-?\\d+?.\\d+),(-?\\d+?.\\d+)\\),\\((-?\\d+?.\\d+),(-?\\d+?.\\d+)\\)\\)";
        //patternString = "timeIntervals:(\\d+.?\\d+)-(\\d+.?\\d+)";
        //patternString = "framesPerSecond:(\\d+)";
        //patternString = "(-?\\d+?.\\d+)\\s+(-?\\d+?.\\d+)"; <--- velocidades
        //patternString = "(\\d+)\\s+(-?\\d+?.\\d+)\\s+(-?\\d+?.\\d+)... ; <--- puntos del objeto

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(path + "\\original.txt", true))) {
            bw.write("marco:(" + dimensiones.get(0).getFirst() + "," + dimensiones.get(0).getSecond() + "),(" + dimensiones.get(1).getFirst() + "," + dimensiones.get(1).getSecond() + ")");
            bw.newLine();
            bw.write("timeIntervals:0.0-" + simulationTime);
            bw.newLine();
            bw.write("framesPerSecond:" + timeStep);
            bw.newLine();
            for (int i = 0; i < cantObjetos; i++) {
                Pair<Double> velocidades = listaObjetos.get(i).getVelocidad();
                bw.write(velocidades.getFirst() + " " + velocidades.getSecond());
                bw.newLine();

                List<Point2D> listaPuntos = listaObjetos.get(i).getListaPuntos();
                StringBuilder pointsLine = new StringBuilder();
                pointsLine.append(listaPuntos.size()).append(" ");
                listaPuntos.stream().forEach((p) -> {
                    pointsLine.append(p.getCoords(0)).append(" ").append(p.getCoords(1)).append(" ");
                });
                String str = pointsLine.substring(0, pointsLine.length() - 1);
                bw.write(str);
                bw.newLine();
            }
        } catch (IOException ex) {
            out.println("Some error here:" + ex.getMessage());
        }
        ////////////////////////////////////////////////////////////
        if (verbose) {
            out.println("Leyendo modelo KDS-SP");
        }

        // genero simulacion cinética
        Simulation smk = new SimulationKinetic(listaPuntosObjetos, velocidadesPorObjeto, 0.0, simulationTime, dimensiones, verbose);
        smk.exec();
        smk.writeStatisticsToFile(path, "\\ksp-statistics.txt");

        // leyendo archivo para SP-Incremental
        if (verbose) {
            out.println("Leyendo modelo SP estático incremental");
        }

        Simulation simulaRealSP = new SimulationStaticIncrementalSweepAndPrune(listaPuntosObjetos, velocidadesPorObjeto, timeStep, 0.0, simulationTime, verbose);
        simulaRealSP.exec();        
        
        // leyendo archivo para SP-Directo
        if (verbose) {
            out.println("Leyendo modelo SP estático directo");
        }

        Simulation smStaticSP = new SimulationStaticDirectSweepAndPrune(listaPuntosObjetos, dimensiones, velocidadesPorObjeto, timeStep, 0.0, simulationTime, verbose);
        smStaticSP.exec();

        out.println("Fin de la simulacion");
    }
}
